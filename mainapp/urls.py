from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('reportuser/create', views.create_report_from_user, name='create_report_from_user'),
    path('reportuser/<int:reportuser_id>/processed', views.processed_report_from_user,
         name='processed_report_from_user'),
    path('reportuser/<int:reportuser_id>/delete', views.delete_report_from_user, name='delete_report_from_user')
]
