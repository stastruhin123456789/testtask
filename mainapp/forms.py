from .models import ReportUser
from django.forms import ModelForm, TextInput, Textarea


class ReportUserForm(ModelForm):
    class Meta:
        model = ReportUser
        fields = ['name', 'email', 'phone', 'comment']

        widgets = {
            "name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите имя'
            }),
            "email": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите почту',
                'type': 'email'
            }),
            "phone": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите номер телефона',
                'type': 'tel'
            }),
            "comment": Textarea(attrs={
                'class': 'form-control',
                'id': 'comment'
            })
        }