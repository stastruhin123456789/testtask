# Generated by Django 3.1.4 on 2020-12-14 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ReportUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Имя абонента')),
                ('email', models.CharField(max_length=255, verbose_name='Почта')),
                ('phone', models.CharField(max_length=20, verbose_name='Номер телефона')),
                ('comment', models.TextField(verbose_name='Комментарий')),
                ('processed', models.BooleanField(default=False, verbose_name='Обработанная заявка')),
            ],
        ),
    ]
