from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import ReportUser
from .forms import ReportUserForm


# Create your views here.
def index(request):
    report_users = ReportUser.objects.all()
    data = {
        'title': 'Список заявок от абонентов',
        'reportUsers': report_users
    }
    return render(request, '../templates/mainapp/index.html', data)


def create_report_from_user(request):
    error = ''
    form = ReportUserForm
    if request.method == 'POST':
        form = ReportUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            error = 'Форма с ошибкой'

    data = {
        'title': 'Заявка',
        'form': form,
        'error': error
    }
    return render(request, '../templates/mainapp/form.html', data)


def processed_report_from_user(request, reportuser_id=0):
    if reportuser_id == 0:
        return redirect('index')
    data = {
        'success': False,
        'error': ''
    }
    report_user = ReportUser.objects.get(id=reportuser_id)
    if report_user:
        report_user.processed = True
        report_user.save()
        data.update({'success': True})
    else:
        data.update({'error': 'processed'})
    return redirect('index')
    # return JsonResponse(data, safe=False)


def delete_report_from_user(request, reportuser_id=0):
    if reportuser_id == 0:
        return redirect('index')
    data = {
        'success': False,
        'error': ''
    }
    report_user = ReportUser.objects.get(id=reportuser_id)
    if report_user:
        report_user.delete()
        data.update({'success': True})
    else:
        data.update({'error': 'delete'})
    return redirect('index')
    # return JsonResponse(data, safe=False)
