from django.db import models


# Create your models here.
class ReportUser(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя абонента')
    email = models.CharField(max_length=255, verbose_name='Почта')
    phone = models.CharField(max_length=20, verbose_name='Номер телефона')
    comment = models.TextField(verbose_name='Комментарий')
    processed = models.BooleanField(default=False, verbose_name='Обработана')

    def __str__(self):
        return str(self.id)
